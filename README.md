# Carzoned Assignment

- Mock API added - Since, this is randomized, it won't return a correct filtered output.
- Login Credentials
  - user1@example.com/password
  - user2@example.com/password
- Registration does not update in Mock API.
- Basic unit tests.


## Repositories

> UI - https://bitbucket.org/cs1193/t-phonepe.carzoned.app/

> UI Port - 38141

> API - https://bitbucket.org/cs1193/t-phonepe.carzoned.api/

> API Port - 38143


## Update the repo modules

`git submodule update --recursive --remote`

## Running the app

`docker-compose up --build -d`
