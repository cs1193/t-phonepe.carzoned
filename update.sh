#!/bin/sh

git submodule update --recursive --remote

docker-compose up --build -d
